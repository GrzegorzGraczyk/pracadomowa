-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 20 Gru 2021, 16:48
-- Wersja serwera: 10.4.22-MariaDB
-- Wersja PHP: 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `Tresc_pytania` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `A` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `B` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `C` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `D` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `Prawidlowa_odpowiedz` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`Tresc_pytania`, `A`, `B`, `C`, `D`, `Prawidlowa_odpowiedz`) VALUES
('Wlaczenie do kodu skryptu zawartosci pliku egzamin zawierajacego kod PHP wymaga dodania instrukcji', 'include egzamin php', 'fopen egzamin php', 'C. fget egzamin php', 'file connect  egzamin php', 'A'),
('W prezentowanym kodzie PHP w miejscu kropek powinno znale?? si? polecenie', 'mysqli_free_result($zapytanie);', 'mysqli_fetch_row($zapytanie);', 'mysqli_num_rows($zapytanie);', ' mysqli_query($zapytanie);', 'B'),
('W j?zyku PHP zmienna $_GET jest tablic?', 'zwyk?a, zdefiniowana przez tworce strony', 'zdefiniowana przez tworce strony, s?u?aca do przekazywania danych z formularza przez adres strony', 'predefiniowana, u?ywana do gromadzenia wartosci formularza po nag??wkach zlecenia HTTP (danych z formularza nie mo?na zobaczy? w adresie)', 'predefiniowana, u?ywana do przekazywania danych do skrypt?w PHP poprzez adres strony', 'D'),
('W kodzie PHP znak \"//\" oznacza', 'operator dzielenia całkowitego', 'początek komentarza jednoliniowego', 'początek komentarza wieloliniowego', 'operator alternatywy', 'B'),
('Funkcja mysqli_query() zapisana w języku PHP służy do', ' pobrania liczby zwróconych rekordów', 'połączenia z bazą danych', 'wykonania zapytania do bazy danych', 'pobrania jednego wiersza ze wszystkich zwróconych rekordów', 'C'),
('Aby zobaczyć wyniki działania skryptu napisanego w języku PHP, będącego elementem strony WWW, musi być on', 'skompilowany po stronie serwera', 'zinterpretowany po stronie serwera', 'skompilowany po stronie klienta', ' zinterpretowany po stronie klienta', 'B'),
(' Interpreter PHP wygeneruje błąd i nie wykona kodu, jeżeli programista:', 'będzie pisał kod bez wcięć tabulacyjnych', 'pobierze wartość z formularza, w którym pole input nie było wypełnione', 'będzie deklarował zmienne wewnątrz warunku', ' nie postawi średnika po wyrażeniu w instrukcji if, jeśli po nim nastąpiła sekcja else', 'D'),
(' W języku PHP pobrano z bazy danych wyniki działania kwerendy za pomocą polecenia mysqli_query(). Aby otrzymać ze zwróconej kwerendy wierszy danych, należy zastosować polecenie:', 'mysqli_num_rows()', ' mysqli_fetch_row()', 'mysqli_fetch_num_rows()', 'mysqli_set_charset()', 'B'),
(' Błędy interpretacji kodu PHP są zapisane:', ' w logu pod warunkiem ustawienia odpowiedniego parametru w pliku php.ini', ' w oknie edytora, w którym powstaje kod PHP', 'w podglądzie zdarzeń systemu Windows', 'nigdzie, są ignorowanie przez przeglądarkę oraz interpreter kodu PHP', 'A');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
